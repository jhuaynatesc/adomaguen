import { Controller, Get, HttpStatus, Param, Query, Req, Res } from '@nestjs/common';
import { ProvinciaService } from './provincia.service';
import { PaginateDto } from 'src/dto/paginate.dto';
import { DistritoService } from 'src/distrito/distrito.service';

@Controller('provincia')
export class ProvinciaController {
    constructor(
        private provinciaService: ProvinciaService,
        private distritoService: DistritoService,

    ){

    }
    @Get("/list-all")
    async listAll(@Res() res,@Req() req, @Query() query: PaginateDto){
        var provincias=await this.provinciaService.listAll(query);
        return res.status(HttpStatus.OK).json({
            provincias
        })
    }
    @Get("/:id")
    async listById(@Res() res,@Param() params, @Query() query: PaginateDto){
        var distritos=await this.distritoService.listByIdProvincia(params.id,query);
        return res.status(HttpStatus.OK).json({
            distritos
        })
    }
}
