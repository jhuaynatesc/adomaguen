import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Provincias } from './entities/provincia.entity';
import { CreateProvinciaDto } from './dto/create.dto';
import { Repository } from 'typeorm';
import { PaginateDto } from 'src/dto/paginate.dto';

@Injectable()
export class ProvinciaService {
    constructor(
        @InjectRepository(Provincias) private readonly provinciasModel: Repository<Provincias>,
    )
    {

    }
    async create(data:CreateProvinciaDto){
        var newProvincias=this.provinciasModel.create(data);
        return await this.provinciasModel.insert(newProvincias);
    }
    async listOne(){
        var data=await this.provinciasModel.findOne({
            where:{}
        });
        return data;
    }
    async listAll(query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.provinciasModel.findAndCount(
            {
                // where:{
                //     userId: id
                // },
                relations: {
                    departamento: true,
                },
                take:take,
                skip: skip,
            }
        );
        var pages=Math.ceil(total/take);
        return {result,total,current:page,pages};
    }
    async listByIdDepartamento(id:number,query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.provinciasModel.findAndCount(
            {
                where:{
                    departamentoId: id
                },
                // relations: {
                //     departamento: true,
                // },
                take:take,
                skip: skip,
            }
        );
        var pages=Math.ceil(total/take);
        return {result,total,current:page,pages};
    }
}
