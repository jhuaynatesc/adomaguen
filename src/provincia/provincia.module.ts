import { Module } from '@nestjs/common';
import { ProvinciaController } from './provincia.controller';
import { ProvinciaService } from './provincia.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Provincias } from './entities/provincia.entity';
import { Distritos } from 'src/distrito/entities/distrito.entity';
import { DistritoService } from 'src/distrito/distrito.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Provincias,
      Distritos
    ])
  ],
  controllers: [ProvinciaController],
  providers: [ProvinciaService,DistritoService]
})
export class ProvinciaModule {}
