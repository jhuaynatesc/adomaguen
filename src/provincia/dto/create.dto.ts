import { IsNumber, IsOptional, IsString} from "class-validator"

export class CreateProvinciaDto{
    @IsString()
    @IsOptional()
    provincia: string
    @IsNumber()
    @IsOptional()
    id: number
    @IsNumber()
    @IsOptional()
    departamentoId: number
}