import { Departamentos } from 'src/departamento/entities/departamento.entity';
import { Distritos } from 'src/distrito/entities/distrito.entity';
import {Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity()
export class Provincias {
    @PrimaryGeneratedColumn()
    id: number
    @Column({nullable:true})
    provincia: string
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn({nullable:true})
    updatedAt: Date;
    @DeleteDateColumn()
    deletedAt: Date;
    @OneToMany(() => Distritos, (distrito) => distrito.provincia,{
        
    })
    @JoinColumn({ name: 'departamentoId' })
    @Column({nullable:true})
    departamentoId: number
    distritos: Distritos[]
    @ManyToOne(() => Departamentos, (departamento) => departamento.provincias,{
        // onDelete: 'CASCADE',
        // onUpdate:'CASCADE'
    })
    departamento: Departamentos[]
}