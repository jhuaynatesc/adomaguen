import { DistritoInterface } from "src/distrito/interfaces/distrito.interface"

export interface ProvinciaInterface {
    readonly provincia: string
    readonly distritos: DistritoInterface[]
}
