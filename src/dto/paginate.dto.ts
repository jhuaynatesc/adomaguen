import { IsNumber, IsOptional, IsString} from "class-validator"

export class PaginateDto{
    @IsString()
    @IsOptional()
    limit: string
    @IsString()
    @IsOptional()
    page: string
    @IsString()
    @IsOptional()
    q: string
}