export interface UserInterface{
    readonly id: number
    readonly name: string
    readonly email: string
    readonly lastname: string
    readonly secondlastname: string
    readonly phone: string
    readonly fullname: string
    readonly token_fcm: string
    readonly verify_number: boolean
    readonly status: boolean
    readonly code_verify: string
    readonly password: string
    // readonly contacts: document
}
export interface UserAuthInterface{
    readonly id: number
    readonly name: string
    readonly email: string
    readonly lastname: string
    readonly secondlastname: string
    readonly phone: string
    readonly status: boolean
    readonly token: string
}
export interface UserAuthPhoneInterface{
    readonly phone: string
    readonly response: object
}