export interface JwtPayload{
    id: number
    email: string
    status: boolean
}