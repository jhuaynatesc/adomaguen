import { Contact } from 'src/contact/entities/contact.entity';
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    name: string
    @Column({nullable:true})
    lastname: string
    @Column({nullable:true})
    secondlastname: string
    @Column({nullable:true})
    fullname: string
    @Column({unique: true,nullable: true})
    phone: string
    @Column({unique: true,nullable: true})
    token_fcm: string
    @Column({unique: true,nullable: true})
    email: string
    @Column({nullable: true})
    password: string
    @Column({nullable: true, length: 5})
    country_code: string
    @Column()
    verify_number: boolean
    @Column()
    status: boolean
    @Column()
    code_verify: string
    @OneToMany(() => Contact, (contact) => contact.id)
    contacts: Contact[]
}
