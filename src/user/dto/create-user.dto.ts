import { IsOptional, IsString, Length } from "class-validator"

export class CreateUserDto{
    @IsString()
    @IsOptional()
    phone: string
    @IsString()
    @IsOptional()
    name?: string
    @IsString()
    @IsOptional()
    email?: string
    @IsString()
    @IsOptional()
    @Length(5,20)
    password?: string
    @IsString()
    @IsOptional()
    lastname?: string
    @IsString()
    @IsOptional()
    secondlastname?: string
}