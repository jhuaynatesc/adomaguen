import { IsOptional, IsString, Length } from "class-validator"

export class LoginUserDto{
    @IsString()
    @IsOptional()
    phone: string
    @IsString()
    @IsOptional()
    email: string
    @IsString()
    @Length(5,20)
    password: string
}
export class CheckUserDto{
    @IsString()
    @IsOptional()
    authorization: string
}

export class LoginPhoneUserDto{
    @IsString()
    phone: string
    @IsString()
    @IsOptional()
    country_code: string
    @IsString()
    @IsOptional()
    code: string
}