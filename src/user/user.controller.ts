import { Body, ConflictException, Controller, Get, Headers, HttpStatus, InternalServerErrorException, NotFoundException, Patch, Post, Req, Res, UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { CheckUserDto, LoginPhoneUserDto, LoginUserDto } from './dto/login-user.dto';

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService
    )
    {

    }
    @Get()
    async listUser(@Res() res){
        var users=await this.userService.listUser();
        return res.status(HttpStatus.OK).json({
            users
        })
    }
    @Patch("/update")
    async updateUser(@Req() req,@Res() res, @Body() dataUser: CreateUserDto){
        var userId=req.user.id;
        var user=await this.userService.updateUser(dataUser,userId);
        if(!user) throw new NotFoundException("User not found");
        return res.status(HttpStatus.OK).json({
            message: "Message sent",
            user
        })
    }
    @Get("/check-status")
    async checkStatusUser(@Res() res, @Headers() userCheck: CheckUserDto){
        var user=await this.userService.checkUser(userCheck);
        if(!user) throw new NotFoundException("User not found");
        return res.status(HttpStatus.OK).json({
            message: "Message sent",
            user
        })
    }
    @Post("/login")
    async loginUser(@Res() res, @Body() login: LoginUserDto){
        var user=await this.userService.loginUser(login);
        if(!user) throw new NotFoundException("User not found");
        return res.status(HttpStatus.OK).json({
            user
        })
    }
    @Post("/login/phone")
    async loginUserPhone(@Res() res, @Body() login: LoginPhoneUserDto){
        try{
            var user=await this.userService.loginUserPhone(login);
            if(!user) throw new NotFoundException("User not found");
            return res.status(HttpStatus.OK).json({
                message: "Message sent",
                user
            })
        }
        catch(error){
            console.log(error);
            if(error?.status==400) throw  new InternalServerErrorException("Error de conexion con el sistema de mensajeria");
            if(error?.status==401) throw  new UnauthorizedException("Error del sistema de mensajeria");
            throw error;
        }
    }
    @Post("/login/phone/verify")
    async loginVerifyUserPhone(@Res() res, @Body() login: LoginPhoneUserDto){
        try{
            var user=await this.userService.loginVerifyUserPhone(login);
            if(!user) throw new NotFoundException("User not found");
            return res.status(HttpStatus.OK).json({
                message: "Codigo verificado",
                user
            })
        }
        catch(error){
            if(error.status==404) throw  new InternalServerErrorException("Error de verificación");
            throw error;
        }
    }
    
    @Post()
    async createUser(@Res() res, @Body() newUser: CreateUserDto){
        try{
            var user=await this.userService.createUser(newUser);
            return res.status(HttpStatus.OK).json({
                message: "User created successfully",
                user
            })
        }
        catch(error){
            console.log(error.code)
            if(error.code=="ER_DUP_ENTRY"){
                throw new ConflictException("User already exists");
            }
            throw error;
        }
    }
}
