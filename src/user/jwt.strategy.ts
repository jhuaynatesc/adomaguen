import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { Strategy } from "passport-local";
import { User } from "./entities/user.entity";

import {ExtractJwt} from "passport-jwt";
import { Repository } from "typeorm";
import { UserInterface } from "./interfaces/user.interface";
import { JwtPayload } from "./jwt-payload.interface";
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    constructor(
        @InjectRepository(User)  private readonly userModel: Repository<User>
    )
    {
        super({
            secretOrKey: "super-secret",
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
        });
    }
    async validate(payload: JwtPayload): Promise<UserInterface>{
        const {email}= payload;
        const user=this.userModel.findOne({
            where: {
                email: email
            }
        })
        if(!user) throw new UnauthorizedException();
        return user;
    }
}