import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UserAuthInterface, UserAuthPhoneInterface, UserInterface } from './interfaces/user.interface';
import { CheckUserDto, LoginPhoneUserDto, LoginUserDto } from './dto/login-user.dto';
import { EncoderService } from './encoder.service';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { Twilio } from 'twilio';

@Injectable()
export class UserService {
    private twilioClient: Twilio;
    constructor(
        @InjectRepository(User) private readonly userModel: Repository<User>,
        private econderService: EncoderService,
        private jwtService: JwtService,
    )
    {
        const accountSid = process.env.TWILIO_ACCOUNT_SID;
        const authToken = process.env.TWILIO_AUTH_TOKEN;
        this.twilioClient = new Twilio(accountSid, authToken);
    }
    async listUser(): Promise<UserInterface[]>{
        const users=await this.userModel.find();
        return users;
    }
    
    async updateUser(dataUser: CreateUserDto, id:number): Promise<UserInterface>{
        console.log(id)
        await this.userModel.update(
            id,
            {
                name: dataUser.name,
                lastname: dataUser.lastname,
                secondlastname: dataUser.secondlastname
            }, 
        );
        var user=await this.userModel.findOne({
            where: {
              id:id
            }
        });
        return user;
    }
    async checkUser(user: CheckUserDto): Promise<UserAuthInterface>{
        console.log(user)

        var jwtService= new JwtService()
        const payload: JwtPayload =  jwtService.decode(user.authorization);
        console.log(payload)
        var userAuth=await this._findUser({id: payload.id});
        console.log(userAuth);
        return userAuth;
    }
    async loginUser(login: LoginUserDto): Promise<UserAuthInterface>{
        const user=await this.userModel.findOne({
            where: {
                email: login.email,
            }
        });
        if(user && (await this.econderService.checkPassword(login.password, user.password))) {
            const payload: JwtPayload={
                id: user.id,
                email: user.email,
                status: user.status
            }
            const accessToken= this.jwtService.sign(payload);
            const userAuth: UserAuthInterface={
                id: user.id,
                email: user.email,
                status: user.status,
                phone: user.phone,
                token: accessToken,
                lastname: user.lastname,
                secondlastname: user.secondlastname,
                name: user.name,
            }
            return userAuth;
        }
        throw new UnauthorizedException("Please check your credentials");
    }
    async loginVerifyUserPhone(login: LoginPhoneUserDto): Promise<UserAuthInterface>{
        var phone =  !login.country_code?`+51${login.phone}`: login.country_code?.concat(login.phone);
        console.log("*************************");
        console.log(phone,login.code);
        const serviceSid = process.env.TWILIO_SERVICE_SID
        var responseTwilio=await this.twilioClient.verify.v2
        .services(serviceSid)
        .verificationChecks.create({ to: phone, code: login.code })
        if(!responseTwilio) throw new NotFoundException("Codigo ya utilizado")
        if(!responseTwilio.valid)  throw new NotFoundException("Codigo invalido");
        
        var userAuth=await this._findUser({phone: login.phone});
        return userAuth;

    }
    async _findUser({phone=null,id=null}): Promise<UserAuthInterface>{
        var query;
        if(phone){
            query={
                phone:phone
            }
        }
        if(id){
            query={
                id:id
            } 
        }
        var user=await this.userModel.findOne({
            where: {
              ...query
            }
        });
        const payload: JwtPayload={
            id: user.id,
            email: user.email,
            status: user.status
        }
        const accessToken= this.jwtService.sign(payload);
        const userAuth: UserAuthInterface={
            id: user.id,
            email: user.email,
            status: user.status,
            phone: user.phone,
            token: accessToken,
            lastname: user.lastname,
            secondlastname: user.secondlastname,
            name: user.name,
        }
        return userAuth;
    }
    async loginUserPhone(login: LoginPhoneUserDto): Promise<UserAuthPhoneInterface>{
        console.log("=========================================")
        login.country_code=!login.country_code?`+51`: login.country_code;
        var phone = login.country_code?.concat(login.phone);
        const serviceSid = process.env.TWILIO_SERVICE_SID
        console.log("=========================================")
        await this.twilioClient.verify.v2.services(serviceSid).verifications.create({ to: phone, channel: 'whatsapp' })
        console.log("=========================================2")
        var userPhone:UserAuthPhoneInterface={
            phone: login.phone,
            response: {}
        }
        var user=await this.userModel.findOne({
            where: {
                phone: login.phone
            }
        });
        console.log("=========================================3")
        if(!user) {
            console.log("=========================================4")
            const newUser=this.userModel.create(login);
            await this.userModel.save(newUser);
            console.log("=========================================5")

        }
        console.log("=========================================6")
        return userPhone;
    }
    async createUser(user: CreateUserDto): Promise<UserInterface>{
        user.password=await this.econderService.encodePassword(user.password);
        const newUser=this.userModel.create(user);
        return await this.userModel.save(newUser);
    }

}
