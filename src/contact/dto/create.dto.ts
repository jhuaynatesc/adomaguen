import { IsNumber, IsOptional, IsString, Length } from "class-validator"
import { TelephoneDto } from "src/telephone/dto/telephone.dto"
export class CreateContactDto{
    @IsString()
    name: string
    @IsString()
    @IsOptional()
    lastname: string
    @IsString()
    @IsOptional()
    secondlastname: string
    @IsString()
    @IsOptional()
    userId: number
    @IsOptional()
    telephones: TelephoneDto[]
}