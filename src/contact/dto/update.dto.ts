import { PartialType } from "@nestjs/mapped-types"
import { IsNumber, IsOptional, IsString, Length } from "class-validator"
import { TelephoneDto } from "src/telephone/dto/telephone.dto"
import { CreateContactDto } from "./create.dto"
export class UpdateContactDto extends PartialType(CreateContactDto){}