import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contact } from './entities/contact.entity';
import { Repository } from 'typeorm';
import { ContactInterface } from './interfaces/contact.interface';
import { CreateContactDto } from './dto/create.dto';
import { TelephoneService } from 'src/telephone/telephone.service';
import { PaginateDto } from 'src/dto/paginate.dto';
import { TelephoneInterface } from 'src/telephone/interfaces/thelephone.interface';
import { UpdateContactDto } from './dto/update.dto';

@Injectable()
export class ContactService {
    constructor(
        @InjectRepository(Contact) private readonly contactModel: Repository<Contact>,
        private telephoneService: TelephoneService
    )
    {
    }
    async create(contact:CreateContactDto): Promise<ContactInterface>{
        var newContact=this.contactModel.create(contact);
        console.log(newContact)
        var data=await this.contactModel.save(newContact);
        var dataTelephones=[]
        for (const telephone of contact.telephones) {
            telephone.contactId=data.id;
            var telephoneModel=await this.telephoneService.create(telephone);
            dataTelephones.push(telephoneModel)
        }
        data.telephones=dataTelephones
        return data;
    }
    async update(id: number,contact:UpdateContactDto): Promise<ContactInterface>{

        await this.contactModel.update(
            id,
            {
                name: contact.name,
                lastname: contact.lastname,
                secondlastname: contact.secondlastname,
            }, 
        );
        var dataTelephones=[]
        for (const telephone of contact.telephones) {
            var telephoneService=await this.telephoneService.listById(telephone.id);
            var telephoneModel: TelephoneInterface;
            if(telephoneService){
                telephoneModel=await this.telephoneService.updateOne(telephone.id,telephone);
            }
            else{
                telephone.contactId=id;
                telephoneModel=await this.telephoneService.create(telephone);
            }
            dataTelephones.push(telephoneModel)
        }
        var data=await this.contactModel.findOne({
            where: {
                id:id
            },
            relations: {
                telephones: true,
            },
        });
        // contact.telephones=dataTelephones;
        return data;
    }
    async listAll(id:number, query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        console.log(id)
        const [result, total]=await this.contactModel.findAndCount(
            {
                where:{
                    userId: id
                },
                relations: {
                    telephones: true,
                },
                take:take,
                skip: skip,
            }
        );
        return {result,total,current:page};
    }
    async list(id:number): Promise<ContactInterface>{
        const contact=await this.contactModel.findOne(
            {
                where:{
                    userId: id
                },
                relations: {
                    telephones: true,
                },
            }
        );
        return contact;
    }
    async listById(id:number): Promise<ContactInterface>{
        const contact=await this.contactModel.findOne(
            {
                where:{
                    id: id
                },
                relations: {
                    telephones: true,
                },
            }
        );
        return contact;
    }
    
}
