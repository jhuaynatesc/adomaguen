import { TelephoneInterface } from "src/telephone/interfaces/thelephone.interface"

export interface ContactInterface{
    readonly name: string
    readonly lastname: string
    readonly secondlastname: string
    readonly telephones: TelephoneInterface[]
}