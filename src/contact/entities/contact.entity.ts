import { TelephoneNumbers } from 'src/telephone/entities/telephone.entity';
import { User } from 'src/user/entities/user.entity';
import {Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Contact {
    @PrimaryGeneratedColumn()
    id: number
    @Column({nullable:true})
    name: string
    @Column({nullable:true})
    lastname: string
    @Column({nullable:true})
    secondlastname: string
    @OneToMany(() => TelephoneNumbers, (phone) => phone.contact,{
        // onDelete: 'CASCADE',
        // onUpdate:'CASCADE'
    })
    telephones: TelephoneNumbers[]
    @ManyToOne(() => User, (user) => user.contacts)
    user: User
    @Column({nullable:true})
    userId: number

    @DeleteDateColumn()
    deletedAt: Date;
}