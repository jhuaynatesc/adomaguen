import { Module } from '@nestjs/common';
import { ContactController } from './contact.controller';
import { ContactService } from './contact.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contact } from './entities/contact.entity';
import { TelephoneService } from 'src/telephone/telephone.service';
import { TelephoneNumbers } from 'src/telephone/entities/telephone.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Contact,
      TelephoneNumbers
    ])
  ],
  controllers: [ContactController],
  providers: [ContactService,TelephoneService]
})
export class ContactModule {}
