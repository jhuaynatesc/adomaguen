import { Body, Controller,Get, HttpStatus, NotFoundException, Param, Post, Put, Query, Req, Res } from '@nestjs/common';
import { CreateContactDto } from './dto/create.dto';
import { ContactService } from './contact.service';
import { PaginateDto } from 'src/dto/paginate.dto';
import { UpdateContactDto } from './dto/update.dto';

@Controller('contact')
export class ContactController {
    constructor(
        private contactService: ContactService
    ){

    }
    @Put("/:id")
    async update(@Req() req,@Res() res, @Body() data: UpdateContactDto, @Param() params){
        data.userId=req.user.id;
        console.log(data)
        var contact=await this.contactService.update(params.id,data);
        if(!contact) throw new NotFoundException("Telephone not found");
        return res.status(HttpStatus.OK).json({
            contact
        })
    }
    @Post()
    async create(@Req() req,@Res() res, @Body() data: CreateContactDto){
        // console.log(req.user)
        data.userId=req.user.id;
        console.log(data)
        var contact=await this.contactService.create(data);
        if(!contact) throw new NotFoundException("Telephone not found");
        return res.status(HttpStatus.OK).json({
            contact
        })
    }
    
    @Get("/list-all")
    async listAll(@Res() res,@Req() req, @Query() query: PaginateDto){
        var contact=await this.contactService.listAll(req.user.id,query);
        return res.status(HttpStatus.OK).json({
            contact
        })
    }
    @Get("/:id")
    async listById(@Param() params,@Res() res){
        try{
            var contact=await this.contactService.listById(params.id);
            if(!contact) throw new NotFoundException("Contact not found");
            return res.status(HttpStatus.OK).json({
                contact
            })
        }
        catch(error){
            throw error;
        }
    }
    @Get()
    async list(@Req() req,@Res() res){
        var contact=await this.contactService.list(req.user.id);
        return res.status(HttpStatus.OK).json({
            contact
        })
    }
}
