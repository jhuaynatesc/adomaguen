import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import 'dotenv/config'; // <- this line is the important

const config: TypeOrmModuleOptions = {
    type: "mysql",
    host: process.env.DB_HOSTNAME||"localhost",
    port: 3306,
    username: process.env.DB_USER||"root",
    password: process.env.DB_PASS||"",
    database: process.env.DB_NAME||"db_contacts",
    entities: [
    __dirname+"/../**/**/*.entity{.ts,.js}"
    ],
    synchronize: true,
};
export const typeOrmConfig = config;