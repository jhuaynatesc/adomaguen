import { Module } from '@nestjs/common';
import { DistritoController } from './distrito.controller';
import { DistritoService } from './distrito.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Distritos } from './entities/distrito.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Distritos
    ])
  ],
  controllers: [DistritoController],
  providers: [DistritoService]
})
export class DistritoModule {}
