import { Controller, Get, HttpStatus, Query, Req, Res } from '@nestjs/common';
import { DistritoService } from './distrito.service';
import { PaginateDto } from 'src/dto/paginate.dto';

@Controller('distrito')
export class DistritoController {
    constructor(
        private distritoService: DistritoService
    ){

    }
    @Get("/list-all")
    async listAll(@Res() res,@Req() req, @Query() query: PaginateDto){
        var distritos=await this.distritoService.listAll(query);
        return res.status(HttpStatus.OK).json({
            distritos
        })
    }
}
