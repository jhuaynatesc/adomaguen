import { Provincias } from 'src/provincia/entities/provincia.entity';
import { TelephoneNumbers } from 'src/telephone/entities/telephone.entity';
import {Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity()
export class Distritos {
    @PrimaryGeneratedColumn()
    id: number
    @Column({nullable:true})
    distrito: string
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn({nullable:true})
    updatedAt: Date;
    @DeleteDateColumn()
    deletedAt: Date;
    @ManyToOne(() => Provincias, (provincia) => provincia.distritos,{
        // onDelete: 'CASCADE',
        // onUpdate:'CASCADE'

    })
    @JoinColumn({ name: 'provinciaId' })
    provincia: Provincias
    @OneToMany(() => TelephoneNumbers, (telephone) => telephone.distrito,{
        // onDelete: 'CASCADE',
        // onUpdate:'CASCADE'
    })
    telephones: TelephoneNumbers[]
    @Column({nullable:true})
    provinciaId: number
}