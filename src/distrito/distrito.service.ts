import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDistritoDto } from './dto/create.dto';
import { Distritos } from './entities/distrito.entity';
import { PaginateDto } from 'src/dto/paginate.dto';

@Injectable()
export class DistritoService {
    constructor(
        @InjectRepository(Distritos) private readonly distritosModel: Repository<Distritos>,
    )
    {

    }
    async create(data:CreateDistritoDto){
        var newProvincias=this.distritosModel.create(data);
        return await this.distritosModel.insert(newProvincias);
    }
    async listOne(){
        var data=await this.distritosModel.findOne({
            where:{}
        });
        return data;
    }
    async listAll(query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.distritosModel.findAndCount(
            {
                // where:{
                //     userId: id
                // },
                relations: {
                    provincia: true,
                },
                take:take,
                skip: skip,
            }
        );
        var pages=Math.ceil(total/take);
        return {result,total,current:page,pages};
    }
    async listByIdProvincia(id:number,query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.distritosModel.findAndCount(
            {
                where:{
                    provinciaId: id
                },
                // relations: {
                //     departamento: true,
                // },
                take:take,
                skip: skip,
            }
        );
        var pages=Math.ceil(total/take);
        return {result,total,current:page,pages};
    }
}
