import { IsNumber, IsOptional, IsString} from "class-validator"

export class CreateDistritoDto{
    @IsString()
    @IsOptional()
    distrito: string
    @IsNumber()
    @IsOptional()
    id: number
}