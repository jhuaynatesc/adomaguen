import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Departamentos } from './entities/departamento.entity';
import { Repository } from 'typeorm';
import { CreateDepartamentoDto } from './dto/create.dto';
import { DepartamentoInterface } from './interfaces/departamento.interface';
import { PaginateDto } from 'src/dto/paginate.dto';

@Injectable()
export class DepartamentoService {
    constructor(
        @InjectRepository(Departamentos) private readonly departamentoModel: Repository<Departamentos>,
    )
    {

    }
    async create(contact:CreateDepartamentoDto){
        var newDepartamento=this.departamentoModel.create(contact);
        var data=await this.departamentoModel.insert(newDepartamento);
        return data;
    }
    async listOne(){
        var data=await this.departamentoModel.findOne({
            where:{}
        });
        return data;
    }
    async listAll(query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.departamentoModel.findAndCount(
            {
                // where:{
                //     userId: id
                // },
                // relations: {
                //     telephones: true,
                // },
                take:take,
                skip: skip,
            }
        );
        var pages=Math.ceil(total/take);
        return {result,total,current:page,pages};
    }
}
