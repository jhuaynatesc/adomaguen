import { IsNumber, IsOptional, IsString} from "class-validator"

export class CreateDepartamentoDto{
    @IsString()
    @IsOptional()
    departamento: string
    @IsNumber()
    @IsOptional()
    id: number
}