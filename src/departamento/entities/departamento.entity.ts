import { Provincias } from 'src/provincia/entities/provincia.entity';
import {Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity()
export class Departamentos {
    @PrimaryGeneratedColumn()
    id: number
    @Column({nullable:true})
    departamento: string
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn({nullable:true})
    updatedAt: Date;
    @DeleteDateColumn()
    deletedAt: Date;
    @OneToMany(() => Provincias, (provincia) => provincia.provincia,{
        // eager: true,
        // cascade: true,
    })
    // @JoinColumn({ name: 'contactId' })
    provincias: Provincias[]
}