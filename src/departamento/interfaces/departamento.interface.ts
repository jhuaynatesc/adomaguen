import { ProvinciaInterface } from "src/provincia/interfaces/provincia.interface"

export interface DepartamentoInterface {
    readonly departamento: string
    readonly provincias: ProvinciaInterface[]
}
