import { Controller, Get, HttpStatus, Param, Query, Req, Res } from '@nestjs/common';
import { DepartamentoService } from './departamento.service';
import { PaginateDto } from 'src/dto/paginate.dto';
import { ProvinciaService } from 'src/provincia/provincia.service';

@Controller('departamento')
export class DepartamentoController {
    constructor(
        private departamentoService: DepartamentoService,
        private provinciaService: ProvinciaService,
    ){

    }
    @Get("/list-all")
    async listAll(@Res() res,@Req() req, @Query() query: PaginateDto){
        var departamentos=await this.departamentoService.listAll(query);
        return res.status(HttpStatus.OK).json({
            departamentos
        })
    }

    @Get("/:id")
    async listById(@Res() res,@Param() params, @Query() query: PaginateDto){
        console.log(params.id)
        console.log(query)
        var provincias=await this.provinciaService.listByIdDepartamento(parseInt(params.id),query);
        console.log("=========================================")
        console.log(provincias)
        console.log("=========================================")
        return res.status(HttpStatus.OK).json({
            provincias
        })
    }
}
