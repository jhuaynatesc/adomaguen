import { Module } from '@nestjs/common';
import { DepartamentoController } from './departamento.controller';
import { DepartamentoService } from './departamento.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Departamentos } from './entities/departamento.entity';
import { ProvinciaService } from 'src/provincia/provincia.service';
import { Provincias } from 'src/provincia/entities/provincia.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Departamentos,
      Provincias,
    ])
  ],
  controllers: [DepartamentoController],
  providers: [DepartamentoService,ProvinciaService]
})
export class DepartamentoModule {}
