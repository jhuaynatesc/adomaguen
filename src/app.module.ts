import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppService } from './app.service';
import { ContactModule } from './contact/contact.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { TelephoneModule } from './telephone/telephone.module';
import { ApiTokenCheckMiddleware } from './middleware/api-token-check.middleware';
import { DepartamentoModule } from './departamento/departamento.module';
import { ProvinciaModule } from './provincia/provincia.module';
import { DistritoModule } from './distrito/distrito.module';
import { DepartamentoService } from './departamento/departamento.service';
import { ProvinciaService } from './provincia/provincia.service';
import { DistritoService } from './distrito/distrito.service';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Departamentos } from './departamento/entities/departamento.entity';
import { Distritos } from './distrito/entities/distrito.entity';
import { Provincias } from './provincia/entities/provincia.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    DatabaseModule,
    ContactModule,
    UserModule,
    TelephoneModule,
    DepartamentoModule,
    ProvinciaModule,
    DistritoModule,
    TypeOrmModule.forFeature([
      Departamentos,
      Distritos,
      Provincias
    ])
  ],
  controllers: [AppController],
  providers: [ConfigService,AppService,DepartamentoService,ProvinciaService,DistritoService],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ApiTokenCheckMiddleware)
    .exclude(
      { path: 'user/login', method: RequestMethod.POST },
      { path: 'user/login/(.*)', method: RequestMethod.POST },
    )
    .forRoutes({path: "*", method: RequestMethod.ALL})
  }
}
