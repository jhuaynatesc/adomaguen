import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import { Departamentos } from './departamento/entities/departamento.entity';
import { Repository } from 'typeorm';
import { DepartamentoService } from './departamento/departamento.service';
import { ProvinciaService } from './provincia/provincia.service';
import { DistritoService } from './distrito/distrito.service';
import { CreateDepartamentoDto } from './departamento/dto/create.dto';
import { CreateProvinciaDto } from './provincia/dto/create.dto';
import { CreateDistritoDto } from './distrito/dto/create.dto';

@Injectable()
export class AppService {
  constructor(
    private departamentoService: DepartamentoService,
    private provinciaService: ProvinciaService,
    private distritoService: DistritoService,
  )
  {

  }
  async defaultDepartamento() {
    var ruta=__dirname+'/storage/private/';
    console.log(ruta);
    var departamentosJSON : CreateDepartamentoDto[]=JSON.parse(await fs.readFileSync(ruta+'departamentos.json', 'utf8'));
    var provinciasJSON: CreateProvinciaDto[]=JSON.parse(await fs.readFileSync(ruta+'provincias.json', 'utf8'));
    var distritosJSON: CreateDistritoDto []=JSON.parse(await fs.readFileSync(ruta+'distritos.json', 'utf8'));
    // var departamentos=[]
    var listDepartamento=await this.departamentoService.listOne();
    if(!listDepartamento){
      for (const item of departamentosJSON) {
        this.departamentoService.create(item);
      }
    }
    var listProvincia=await this.provinciaService.listOne();
    if(!listProvincia){
      for (const item of provinciasJSON) {
        this.provinciaService.create(item);
      }
    }
    var listDistrito= await this.distritoService.listOne();
    if(!listDistrito){
      for (const item of distritosJSON) {
        this.distritoService.create(item);
      }
    }
    return true;
  }
}
