import { Contact } from 'src/contact/entities/contact.entity';
import { Distritos } from 'src/distrito/entities/distrito.entity';
import { Provincias } from 'src/provincia/entities/provincia.entity';
import {Column, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';

export enum TypeNumbers {
    Contact = 'contact',
    Global = 'global',
    Private = 'private',
    Local = 'local'
}

@Entity()
export class TelephoneNumbers {
    @PrimaryGeneratedColumn()
    id: number
    @Column({type: "varchar",nullable:true})
    type: TypeNumbers
    @Column({nullable:true})
    phone: string
    @ManyToOne(() => Contact, (contact) => contact.telephones,{
        // eager: true,
        // cascade: true,
    })
    @JoinColumn({ name: 'contactId' })
    contact: Contact
    @Column({type: "float",precision: 11, scale: 7, nullable:true})
    latitude: number
    @Column({type: "float",precision: 11, scale: 7, nullable:true})
    longitude: number
    @Column({nullable:true})
    ubigeo: string
    @Column({nullable:true})
    name: string
    @Column({nullable:true})
    contactId: number

    @DeleteDateColumn()
    deletedAt: Date;
    
    @ManyToOne(() => Distritos, (distrito) => distrito.telephones,{
        // onDelete: 'CASCADE',
        // onUpdate:'CASCADE'
    })
    @JoinColumn({ name: 'distritoId' })
    distrito: Distritos
    @Column({nullable:true})
    distritoId: number
}
