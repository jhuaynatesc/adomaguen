import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Patch, Post, Query, Req, Res } from '@nestjs/common';
import { CreateTelephoneDto } from './dto/create.dto';
import { TelephoneService } from './telephone.service';
import { PaginateDto } from 'src/dto/paginate.dto';
import { SearchTelephoneDto } from './dto/search.dto';
import { UpdateTelephoneDto } from './dto/update.dto';

@Controller('telephone')
export class TelephoneController {
    constructor(
        private telephoneService: TelephoneService
    ){

    }
    @Patch("/:id")
    async update(@Req() req,@Res() res, @Body() data: UpdateTelephoneDto, @Param() params){
        var contact=await this.telephoneService.update(params.id,data);
        if(!contact) throw new NotFoundException("Telephone not found");
        return res.status(HttpStatus.OK).json({
            contact
        })
    }
    @Post()
    async create(@Res() res, @Body() data: CreateTelephoneDto,@Req() req){
        var telephone=await this.telephoneService.create(data,parseInt(req.user.id));
        if(!telephone) throw new NotFoundException("Telephone not found");
        return res.status(HttpStatus.OK).json({
            telephone
        })
    }
    @Delete('/:id')
    async delete(@Res() res, @Param() params){
        var telephones=await this.telephoneService.delete(params.id);
        return res.status(HttpStatus.OK).json({
            telephones
        })
    }
    @Get("/list-all")
    async listAll(@Res() res, @Query() query: PaginateDto){
        var telephones=await this.telephoneService.listAll(query);
        return res.status(HttpStatus.OK).json({
            telephones
        })
    }
    @Get("/list-all/:type")
    async listAllForType(@Res() res, @Query() query: PaginateDto,@Param() params: SearchTelephoneDto){
        var telephones=await this.telephoneService.listAllForType(query,params);
        return res.status(HttpStatus.OK).json({
            telephones
        })
    }
}
