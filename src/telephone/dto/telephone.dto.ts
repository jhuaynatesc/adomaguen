import { IsNumber, IsOptional, IsString, Length } from "class-validator"
import { TypeNumbers } from "../entities/telephone.entity"

export class TelephoneDto{
    @IsString()
    @IsOptional()
    id: number
    @IsString()
    type: TypeNumbers
    @IsString()
    phone: string
    @IsNumber()
    @IsOptional()
    latitude: number
    @IsNumber()
    @IsOptional()
    longitude: number
    @IsString()
    @IsOptional()
    ubigeo: string
    @IsString()
    @IsOptional()
    contactId: number
    @IsString()
    @IsOptional()
    name: string
    @IsNumber()
    @IsOptional()
    departamentoId: number
    @IsNumber()
    @IsOptional()
    provinciaId: number
    @IsNumber()
    @IsOptional()
    distritoId: number
}