import { PartialType } from "@nestjs/mapped-types";
import { CreateTelephoneDto } from "./create.dto";

export class UpdateTelephoneDto extends PartialType(CreateTelephoneDto){}