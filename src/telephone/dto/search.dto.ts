import { IsOptional, IsString } from "class-validator"
import { TypeNumbers } from "../entities/telephone.entity"

export class SearchTelephoneDto{
    @IsString()
    @IsOptional()
    type: TypeNumbers
}