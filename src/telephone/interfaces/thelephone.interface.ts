export interface TelephoneInterface{
    readonly id: number
    readonly type: string
    readonly phone: string
    readonly latitude: number
    readonly longitude: number
    readonly ubigeo: string
}