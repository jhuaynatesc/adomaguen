import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TelephoneNumbers } from './entities/telephone.entity';
import { TelephoneInterface } from './interfaces/thelephone.interface';
import { CreateTelephoneDto } from './dto/create.dto';
import { PaginateDto } from 'src/dto/paginate.dto';
import { SearchTelephoneDto } from './dto/search.dto';
import { TelephoneDto } from './dto/telephone.dto';
import { UpdateTelephoneDto } from './dto/update.dto';

@Injectable()
export class TelephoneService {
    constructor(
        @InjectRepository(TelephoneNumbers) private readonly telephoneModel:Repository<TelephoneNumbers>
    )
    {

    }
    async update(id: number,contact:UpdateTelephoneDto): Promise<TelephoneInterface>{
        console.log(contact)
        await this.telephoneModel.update(
            id,
            {
                ...contact
            }, 
        );
        var data=await this.telephoneModel.findOne({
            where: {
                id:id
            },
            relations: {
                distrito: true,
            },
        });
        // contact.telephones=dataTelephones;
        return data;
    }
    async listById(id:number): Promise<TelephoneInterface>{
        try{
            const telephone=await this.telephoneModel.findOne(
                {
                    where:{
                        id: id
                    },
                },
            );
            return telephone;
        }
        catch(e){
            console.log(e)
            return null;
        }
    }
    async delete(id:number){
        const telephone=await this.telephoneModel.softDelete({id});
        return telephone;
    }
    async updateOne(id:number,telephone:TelephoneDto): Promise<TelephoneInterface>{
        await this.telephoneModel.update(id,
            telephone
        );
        const telephoneModel=await this.listById(id)
        return telephoneModel;
    }
    async create(telephone:CreateTelephoneDto, userId ?: number): Promise<TelephoneInterface>{
        
        var newTelephone=this.telephoneModel.create({
            ...telephone,
            // contactId:userId
        });
        return await this.telephoneModel.save(newTelephone);
    }
    async listAll(query : PaginateDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.telephoneModel.findAndCount(
            {
                take:take,
                skip: skip,
                relations: ["distrito", "distrito.provincia","distrito.provincia.departamento"],
                // relations:{
                //     distrito: true
                // }
            },
        );
        var pages=Math.ceil(total/take);
        return {result,total,pages,current:page};
    }
    async listAllForType(query : PaginateDto, params: SearchTelephoneDto){
        const take = parseInt(query.limit) || 1
        const page=parseInt(query.page) || 1;
        const skip= (page-1) * take ;
        const [result, total]=await this.telephoneModel.findAndCount(
            {
                where:{
                    type:params.type,
                    distritoId: parseInt(query.q)
                },
                take:take,
                skip: skip,
            }
        );
        return {result,total,current:page};
    }
}
