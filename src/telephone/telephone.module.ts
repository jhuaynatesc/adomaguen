import { Module } from '@nestjs/common';
import { TelephoneService } from './telephone.service';
import { TelephoneController } from './telephone.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelephoneNumbers } from './entities/telephone.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      TelephoneNumbers
    ])
  ],
  providers: [TelephoneService],
  controllers: [TelephoneController]
})
export class TelephoneModule {}
