import { Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post("/default")
  async defaultData(@Res() res){
    await this.appService.defaultDepartamento();
    return res.status(HttpStatus.OK).json({
        "message": "Cinfiguración realizado"
    });
  }
}
