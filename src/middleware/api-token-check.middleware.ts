
import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request, Response, NextFunction } from 'express';
import { JwtPayload } from 'src/user/jwt-payload.interface';
@Injectable()
export class ApiTokenCheckMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const header = req.headers.authorization;
    if (!header) {
        console.log(header);
        throw new UnauthorizedException("Acceso denegado!");
    } 
    else {
        try {
            var jwtService= new JwtService()
            const payload: JwtPayload =  jwtService.decode(header);
            req.user = payload;
            next();
        }
        catch (error) {
            if (error.name == "TokenExpiredError") {
                throw new UnauthorizedException("Token expirado. Inicia Sesión de nuevo");
            } else if (error.name == "JsonWebTokenError") {
                throw new UnauthorizedException("Token invalid");
            }
            throw new UnauthorizedException(error);
        }
    }
  }
}
